// get information through HTTP/GET
function get_information(link, callback){
  var xhr = new XMLHttpRequest();
  xhr.open("GET", link, true);
  xhr.onreadystatechange = function(){
  if (xhr.readyState === 4) {
    callback(xhr.responseText);
  }};
  xhr.send(null);
}

function report(){
  // send report and get the status code | status code: 1 = Ok; 0 = Error;
  var query = "http://localhost/report.php?domain?=" + document.domain + "&url=" + document.URL + "&conttype=" + document.contentType;
  get_information(query, function(data){
    if(stat_code == "1"){
      alert("Ok, this should be invisible or not annoying.");
    }else if(stat_code == "0"){
      alert("Error sending report!! This should be invisible or not annoying.");
    }
  });
}

// just call report(); it will do the job for you.